import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    initialUsers: [],
    filteredUsers: [],
    modalOpen: false,
    currentEditUser: null,
  },
  mutations: {
    setInitialUsers(state, users) {
      state.initialUsers = users;
    },
    setFileterdUsers(state, users) {
      state.filteredUsers = users;
    },
    setModalOpen(state, value) {
      state.modalOpen = value;
    },
    setCurrentEditUser(state, payload) {
      state.currentEditUser = payload;
    },
  },
  actions: {
    setInitialUsers({ commit }, users) {
      commit("setInitialUsers", users);
      commit("setFileterdUsers", users);
    },
    filterUsersByName({ commit, state }, filter) {
      const filteredUsers = state.initialUsers.filter(
        (user) =>
          user.name.toLowerCase().includes(filter.text.toLowerCase()) &&
          user.active !== filter.status
      );
      commit("setFileterdUsers", filteredUsers);
    },
    filterUsersByStatus({ commit, state }, status) {
      const filteredUsers = state.initialUsers.filter(
        (user) => user.active !== status
      );
      commit("setFileterdUsers", filteredUsers);
    },
    deleteUser({ dispatch, state }, id) {
      const users = state.initialUsers.filter((user) => user.id !== id);
      dispatch("setInitialUsers", users);
    },
    editUser({ commit, state }, id) {
      if (id) {
        const user = state.initialUsers.find((user) => user.id === id);
        commit("setCurrentEditUser", user);
        commit("setModalOpen", true);
        return;
      }
      return commit("setCurrentEditUser", null);
    },
    addUser({ dispatch, state }, user) {
      const users = [...state.initialUsers, user];
      dispatch("setInitialUsers", users);
    },
    addEditedUser({ dispatch, state }, user) {
      const users = state.initialUsers.map((u) =>
        u.id === user.id ? user : u
      );
      dispatch("setInitialUsers", users);
    },
    openModal({ commit }, payload) {
      commit("setModalOpen", payload);
    },
  },
  getters: {
    getFilteredUsers: (state) => state.filteredUsers,
    getModalStatus: (state) => state.modalOpen,
    getCurrentEditUser: (state) => state.currentEditUser,
  },
});
