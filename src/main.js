import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import {
  Button,
  Card,
  Avatar,
  Row,
  Col,
  Icon,
  Tag,
  Input,
  Select,
  Divider,
  Modal,
  FormModel,
  Switch,
  Radio,
} from "ant-design-vue";

Vue.config.productionTip = false;
Vue.use(Button);
Vue.use(Card);
Vue.use(Avatar);
Vue.use(Row);
Vue.use(Col);
Vue.use(Icon);
Vue.use(Tag);
Vue.use(Input);
Vue.use(Select);
Vue.use(Divider);
Vue.use(Modal);
Vue.use(FormModel);
Vue.use(Switch);
Vue.use(Radio);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
